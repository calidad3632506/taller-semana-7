import mundo.Calculadora;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTest {

    @Test
    public void operacionesTotales(){

        Calculadora calculadora = new Calculadora();

        Integer resultado = calculadora.darTotalOperaciones();

        Assertions.assertEquals(1000000,resultado);

    }

    @Test
    public void restaOperaciones(){

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.sumar(2.0,2.0);
        Integer operacionesRestantes = calculadora.darTotalOperaciones();

        Assertions.assertEquals("4.0",resultado);
        Assertions.assertEquals(999999,operacionesRestantes);

    }

    @Test
    public void sinOperacionesRestantes(){

        Calculadora calculadora = new Calculadora();

        while (calculadora.darTotalOperaciones()>0) {

            calculadora.sumar(1.0,1.0);

        }
        String resultado = calculadora.sumar(1.0,1.0);
        Integer operacionesRestantes = calculadora.darTotalOperaciones();

        Assertions.assertEquals("No quedan operaciones disponibles.",resultado);
        Assertions.assertEquals(0,operacionesRestantes);

    }

    @Test
    public void suma(){

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.sumar(1.0,2.0);

        Assertions.assertEquals("3.0",resultado);

    }

    @Test
    public void resta(){

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.restar(3.0,8.0);

        Assertions.assertEquals("-5.0",resultado);

    }

    @Test
    public void multiplicacion(){

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.multiplicar(4.0,2.0);

        Assertions.assertEquals("8.0",resultado);

    }

    @Test
    public void division(){

        Calculadora calculadora = new Calculadora();

        String resultado = calculadora.dividir(9.0,2.0);

        Assertions.assertEquals("4.5",resultado);

    }

}
