package mundo;

public class Main {
    public static void main(String[] args) {

        Calculadora calculadora = new Calculadora();

        System.out.println(calculadora.sumar(2.0, 3.0));
        System.out.println(calculadora.restar(2.0, 3.0));
        System.out.println(calculadora.multiplicar(2.0, 3.0));
        System.out.println(calculadora.dividir(2.0, 3.0));

        System.out.println(calculadora.darTotalOperaciones());

        while (calculadora.darTotalOperaciones()>0) {

            calculadora.sumar(1.0,1.0);

        }

        System.out.println(calculadora.sumar(1.0,1.0));

        System.out.println(calculadora.darTotalOperaciones());

    }

}
