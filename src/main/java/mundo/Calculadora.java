package mundo;

public class Calculadora {

    private Integer totalOperaciones;

    public Calculadora() {

        totalOperaciones = 1000000;

    }

    public String sumar(Double numeroUno, Double numeroDos) {

        if (totalOperaciones > 0) {

            String resultado = Double.toString(numeroUno + numeroDos);
            totalOperaciones = totalOperaciones - 1;
            return resultado;

        } else {

            return "No quedan operaciones disponibles.";

        }

    }

    public String restar(Double numeroUno, Double numeroDos) {

        if (totalOperaciones > 0) {

            String resultado = Double.toString(numeroUno - numeroDos);
            totalOperaciones = totalOperaciones - 1;
            return resultado;

        } else {

            return "No quedan operaciones disponibles.";

        }

    }

    public String multiplicar(Double numeroUno, Double numeroDos) {

        if (totalOperaciones > 0) {

            String resultado = Double.toString(numeroUno * numeroDos);
            totalOperaciones = totalOperaciones - 1;
            return resultado;

        } else {

            return "No quedan operaciones disponibles.";

        }

    }

    public String dividir(Double numeroUno, Double numeroDos) {

        if (totalOperaciones > 0) {

            String resultado = Double.toString(numeroUno / numeroDos);
            totalOperaciones = totalOperaciones - 1;
            return resultado;

        } else {

            return "No quedan operaciones disponibles.";

        }

    }

    public Integer darTotalOperaciones(){

        return totalOperaciones;

    }

}
