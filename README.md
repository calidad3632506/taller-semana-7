**Taller semana 7**

- Crear una calculadora alemana, la cual puede dar visibilidad del tiempo de vida útil que le queda.

- El tiempo de vida está dado por la cantidad de operaciones ejecutadas.

- En total la vida útil es de 1 000 000 de operaciones.

- En cada momento que se hace una operación se resta 1 punto de vida útil.

- Implementar los métodos existentes, las pruebas unitarias para este tipo de calculadora.

- Evaluar casos felices y erróneos.
